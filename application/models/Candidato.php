<?php

    class Candidato extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }
        //Funcion para insertar un servicio
        function insertar($datos){
            return $this->db->insert("candidato", $datos);

        }
        //Funcion para consultar servicios
        function obtenerTodos(){
            $listadoCandidatos=
            $this->db->get("candidato");
            
            if($listadoCandidatos
                ->num_rows()>0){//Si hay datos
                    return $listadoCandidatos->result();
            }else{//No hay datos
                return false;
            }

        }
        //Borrar servicio
        function borrar($id_can){
            $this->db->where("id_can",$id_can);
            return $this->db->delete("candidato"); 
        }

    }//Cierre de la clase
?>