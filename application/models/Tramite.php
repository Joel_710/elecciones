<?php

    class Tramite extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }
        //Funcion para insertar un servicio
        function insertar($datos){
            return $this->db->insert("tramite", $datos);

        }
        //Funcion para consultar servicios
        function obtenerTodos(){
            $listadoTramites=
            $this->db->get("tramite");
            
            if($listadoTramites
                ->num_rows()>0){//Si hay datos
                    return $listadoTramites->result();
            }else{//No hay datos
                return false;
            }

        }
        //Borrar servicio
        function borrar($id_tra){
            $this->db->where("id_tra",$id_tra);
            return $this->db->delete("tramite"); 
        }

    }//Cierre de la clase
?>