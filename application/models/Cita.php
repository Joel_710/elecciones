<?php

    class Cita extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }
        //Funcion para insertar un viaje
        function insertar($datos){
            return $this->db->insert("cita", $datos);

        }
        //Funcion para consultar Viajes
        function obtenerTodos(){
            $listadoCitas=
            $this->db->get("cita");
            
            if($listadoCitas
                ->num_rows()>0){//Si hay datos
                    return $listadoCitas->result();
            }else{//No hay datos
                return false;
            }

        }
        //Borrar Viajes
        function borrar($id_cita){
            $this->db->where("id_cita",$id_cita);
            return $this->db->delete("cita"); 
        }

    }//Cierre de la clase
?>