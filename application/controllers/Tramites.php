<?php
    class Tramites extends CI_Controller
    {

        function __construct()
        {
            parent::__construct();
            //Cargar Modelo
            $this->load->model('Tramite');
        }
        //Funcion que renderiza la vista index                                        
        public function index()
        {
            $data['tramites'] = $this->Tramite->obtenerTodos();
            // print_r($data);
            $this->load->view('header');
            $this->load->view('tramites/index', $data);
            $this->load->view('footer');
        }

        public function nuevo()
        {
            $this->load->view('header');
            $this->load->view('tramites/nuevo');
            $this->load->view('footer');
        }

        public function guardar()
        {
            $datosNuevoTramite = array(
                "id_tra" => $this->input->post('id_tra'),
                "nombre_tra" => $this->input->post('nombre_tra'),
                "cedula_tra" => $this->input->post('cedula_tra'),
                "fecha_tra" => $this->input->post('fecha_tra'),
                "servicio_tra" => $this->input->post('servicio_tra')
            );
            if ($this->Tramite->insertar($datosNuevoTramite)) {
                redirect('tramites/index');
            } else {
                echo "<h1>ERROR DE LA PAGINA</h1>";
            }
        }
        //funcion para eliminar instructores
        public function eliminar($id_tra)
        {
            if ($this->Tramite->borrar($id_tra)) {
                redirect('tramites/index');
            } else {
                echo "ERROR AL BORRAR :(";
            }
        }
    } // Cierre de la clase
?>