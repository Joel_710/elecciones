<?php
    class Contactos extends CI_Controller
    {

      function __construct()
      {
        parent::__construct();
        //Cargar Modelo
        $this->load->model('Contacto');

      }
      //Funcion que renderiza la vista index                                        
      public function index(){
        $data['contactos']=$this->Contacto->obtenerTodos();
        // print_r($data);
        $this->load->view('header');
        $this->load->view('contactos/index',$data);
        $this->load->view('footer');
      }

      public function nuevo(){
        $this->load->view('header');
        $this->load->view('contactos/nuevo');
        $this->load->view('footer');
      }

      public function guardar(){
        $datosNuevoContacto=array("nombres_con"=>$this->input->post('nombres_con'),
        "apellidos_con"=>$this->input->post('apellidos_con'),
        "email_con"=>$this->input->post('email_con'),        
        "telefono_con"=>$this->input->post('telefono_con'),
        "direccion_con"=>$this->input->post('direccion_con')
      );
        if($this->Contacto->insertar($datosNuevoContacto)){
          redirect('contactos/index');

        }else{
          echo "<h1>ERROR DE LA PAGINA</h1>";
        }

      }
      //funcion para eliminar 
      public function eliminar($id_con){
        if($this->Contacto->
        borrar($id_con)) {
        redirect('contactos/index');
        } else {
          echo "ERROR AL BORRAR :(";
        } 
      }
      
    } // Cierre de la clase
?>