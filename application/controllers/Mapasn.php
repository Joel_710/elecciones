<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mapasn extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model("Mapan");
	}

	public function index()
	{
		$data["mapasn"] = $this->Mapan->obtenerTodos();
		$this->load->view('mapasn', $data);
	}
}
