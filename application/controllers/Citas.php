<?php
    class Citas extends CI_Controller
    {

        function __construct()
        {
            parent::__construct();
            //Cargar Modelo
            $this->load->model('Cita');
        }
        //Funcion que renderiza la vista index                                        
        public function index()
        {
            $data['citas'] = $this->Cita->obtenerTodos();
            // print_r($data);
            $this->load->view('header');
            $this->load->view('citas/index', $data);
            $this->load->view('footer');
        }

        public function nuevo()
        {
            $this->load->view('header');
            $this->load->view('citas/nuevo');
            $this->load->view('footer');
        }

        public function guardar(){
            $datosNuevoCita = array("cedula_cita" => $this->input->post('cedula_cita'),
                "nombre_cita" => $this->input->post('nombre_cita'),
                "apellido_cita" => $this->input->post('apellido_cita'),
                "telefono_cita" => $this->input->post('telefono_cita'),
                "direccion_cita" => $this->input->post('direccion_cita')
            );
            if ($this->Cita->insertar($datosNuevoCita)) {
                redirect('citas/index');
            } else {
                echo "<h1>ERROR DE LA PAGINA</h1>";
            }
        }
        //funcion para eliminar 
        public function eliminar($id_cita){
            if ($this->Cita->
            borrar($id_cita)) {
            redirect('citas/index');
            } else {
                echo "ERROR AL BORRAR :(";
            }
        }
    } // Cierre de la clase
?>