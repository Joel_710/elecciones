<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mapaspv extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model("Mapapv");
	}

	public function index()
	{
		$data["mapaspv"] = $this->Mapapv->obtenerTodos();
		$this->load->view('mapaspv', $data);
	}
}
