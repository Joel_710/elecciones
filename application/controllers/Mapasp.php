<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mapasp extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model("Mapap");
	}

	public function index()
	{
		$data["mapasp"] = $this->Mapap->obtenerTodos();
		$this->load->view('mapasp', $data);
	}
}
