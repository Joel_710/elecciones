<?php
class Candidatos extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        //Cargar Modelo
        $this->load->model('Candidato');
    }
    //Funcion que renderiza la vista index                                        
    public function index()
    {
        $data['candidatos'] = $this->Candidato->obtenerTodos();
        // print_r($data);
        $this->load->view('header');
        $this->load->view('candidatos/index', $data);
        $this->load->view('footer');
    }

    public function nuevo()
    {
        $this->load->view('header');
        $this->load->view('candidatos/nuevo');
        $this->load->view('footer');
    }

    public function guardar()
    {
        $datosNuevoCandidato = array(
            "dignidad_can" => $this->input->post('dignidad_can'),
            "apellido_can" => $this->input->post('apellido_can'),
            "nombre_can" => $this->input->post('nombre_can'),
            "latitud_can" => $this->input->post('latitud_can'),
            "longitud_can" => $this->input->post('longitud_can')
        );
        if ($this->Candidato->insertar($datosNuevoCandidato)) {
            redirect('candidatos/index');
        } else {
            echo "<h1>ERROR DE LA PAGINA</h1>";
        }
    }
    //funcion para eliminar instructores
    public function eliminar($id_can)
    {
        if ($this->Candidato->borrar($id_can)) {
            redirect('candidatos/index');
        } else {
            echo "ERROR AL BORRAR :(";
        }
    }
} // Cierre de la clase
