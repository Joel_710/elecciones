<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="<?php echo base_url(); ?>/assets/images/1.jpg" alt="" style="width: 150px; height: 150px;">
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <br>
            <h1 style="color:red;"><b>NUEVO TRAMITE</b></h1>
            <br>
        </div>
    </div>
</div>

<div class="container">
<form class="" action="<?php echo site_url();?>/tramites/guardar" method="post">
    <div class="row">
        <div class="col-md-4 text-center">
        <label for="">TRAMITE:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el Tramite"
        class="form-control" name="nombre_tra" value="">
        </div>
        <div class="col-md-4 text-center">
        <label for="">CEDULA:</label>
        <br>
        <input type="text"
        placeholder="Ingrese su cedula"
        class="form-control" name="cedula_tra" value="">
        </div>
        <div class="col-md-2 text-center">
        <label for="">FECHA:</label>
        <br>
        <input type="text"
        placeholder="Ingrese la fecha"
        class="form-control" name="fecha_tra" value="">
        </div>
        <div class="col-md-4 text-center">
        <label for="">SERVICIO:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el servicio"
        class="form-control" name="servicio_tra" value="">
        </div>
    </div>
    <br>
    <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary">
        GUARDAR
        </button>
        &nbsp;
        <a href="<?php echo site_url(); ?>/tramites/index" class="btn btn-danger">CANCELAR</a>
    </div>
    <br>
</form>
</div>
<br>
<br>