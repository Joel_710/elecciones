<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="<?php echo base_url(); ?>/assets/images/1.jpg" alt="" style="width: 150px; height: 150px;">
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <br>
            <h1 style="color:red;"><b>LISTADO DE TRAMITES</b></h1>
        </div>
    </div>
</div>
<div class="col-md-4">
    <a href="<?php echo site_url('tramites/nuevo'); ?>" class="btn btn-primary">
        <i class="glyphicon glyphicon-plus"></i>
        Agregar Tramite
    </a>
</div>
<br>
<br>
<br>
<?php if ($tramites) : ?>
    <table class="table table=striped table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>TRAMITE</th>
                <th>CEDULA</th>
                <th>FECHA</th>
                <th>SERVICIO</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tramites
                as $filaTemporal) : ?>
                <tr>
                    <td>
                        <?php echo
                        $filaTemporal->id_tra; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->nombre_tra; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->cedula_tra; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->fecha_tra; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->servicio_tra; ?>
                    </td>
                    <td class="text-center">
                        <a href="#" title="Editar Tramite">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url(); ?>/tramites/eliminar/<?php echo $filaTemporal->id_tra; ?>" title="Eliminar Tramite" onclick="return confirm('¿Estas seguro?');" style="color:red;">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    </td>

                </tr>
            <?php endforeach; ?>
        </tbody>

    </table>
<?php else : ?>
    <h1> Dont have Tramites<h1>
        <?php endif; ?>