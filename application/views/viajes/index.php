<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="<?php echo base_url(); ?>/assets/images/01.png" alt="" style="width: 150px; height: 150px;">
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <br>
            <h1 style="color:red;"><b>LISTADO DE VIAJES</b></h1>
        </div>
    </div>
</div>
<div class="col-md-4">
    <a href="<?php echo site_url('viajes/nuevo'); ?>" class="btn btn-primary">
        <i class="glyphicon glyphicon-plus"></i>
        Agregar Viaje
    </a>
</div>
<br>
<br>
<br>
<?php if ($viajes) : ?>
    <table class="table table=striped table-bordered table-hover">
      <thead>
        <tr>
            <th>ID</th>
            <th>EMPRESA</th>
            <th>FECHA</th>
            <th>RUC</th>
            <th>NOMBRES</th>
            <th>APELLIDOS</th>
            <th>TIPO LICENCIA</th>
            <th>TELEFONO</th>
            <th>DIRECCION</th>
            <th>ACCIONES</th>
        </tr>
      </thead>
      <tbody>
            <?php foreach ($viajes
            as $filaTemporal): ?>
              <tr>
                  <td>
                      <?php echo
                      $filaTemporal->id_via; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->empresa_via; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->fecha_via; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->ruc_via; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->nombres_via; ?>
                  </td>
                  <td>
                    <?php echo 
                    $filaTemporal->apellidos_via; ?>
                  </td>
                  <td>
                    <?php echo 
                    $filaTemporal->tipo_via; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->telefono_via; ?>
                  </td>
                  <td>
                      <?php echo
                      $filaTemporal->direccion_via; ?>
                  </td>
                  <td class="text-center">
                    <a href="#" title="Editar Viaje">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo site_url(); ?>/viajes/eliminar/<?php echo $filaTemporal->id_via; ?>" 
                    title="Eliminar Viaje"
                    onclick="return confirm('¿Estas seguro?');"
                    style="color:red;">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                  </td>
                  
              </tr>
            <?php endforeach; ?>
        </tbody>
        
    </table>
<?php else : ?>
    <h1> Dont have Viajes<h1>
        <?php endif; ?>