<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="<?php echo base_url(); ?>/assets/images/1.jpg" alt="" style="width: 150px; height: 150px;">
        </div>
    </div>
</div>




<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <br>
            <h1 style="color:red;"><b>NUEVA CITA</b></h1>
            <br>
        </div>
    </div>
</div>

<div class="container">
    <form class="" action="<?php echo site_url(); ?>/citas/guardar" method="post">
        <div class="row">
            <div class="col-md-4 text-center">
                <label for="">CEDULA:</label>
                <br>
                <input type="text" placeholder="Ingrese su cedula" class="form-control" name="cedula_cita" value="">
            </div>
            <div class="col-md-4 text-center">
                <label for="">NOMBRES:</label>
                <br>
                <input type="text" placeholder="Ingrese sus nombres" class="form-control" name="nombre_cita" value="">
            </div>
            <div class="col-md-4 text-center">
                <label for="">APELLIDOS:</label>
                <br>
                <input type="text" placeholder="Ingrese sus apellidos" class="form-control" name="apellido_cita" value="">
            </div>
            <div class="col-md-4 text-center">
                <label for="">TELÉFONO:</label>
                <br>
                <input type="text" placeholder="Ingrese el teléfono" class="form-control" name="telefono_cita" value="">
            </div>
            <div class="col-md-6 text-center">
                <label for="">DIRECCIÓN:</label>
                <br>
                <input type="text" placeholder="Ingrese la dirección" class="form-control" name="direccion_cita" value="">
            </div>
        </div>
        <br>
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-primary">
                GUARDAR
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/citas/index" class="btn btn-danger">CANCELAR</a>
        </div>
        <br>
    </form>
</div>
<br>
<br>