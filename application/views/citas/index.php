<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="<?php echo base_url(); ?>/assets/images/1.jpg" alt="" style="width: 150px; height: 150px;">
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <br>
            <h1 style="color:red;"><b>LISTADO DE CITAS</b></h1>
        </div>
    </div>
</div>
<div class="col-md-4">
    <a href="<?php echo site_url('citas/nuevo'); ?>" class="btn btn-primary">
        <i class="glyphicon glyphicon-plus"></i>
        Agregar Cita
    </a>
</div>
<br>
<br>
<br>
<?php if ($citas) : ?>
    <table class="table table=striped table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>CEDULA</th>
                <th>NOMBRE</th>
                <th>APELLIDO</th>
                <th>TELEFONO</th>
                <th>DIRECCION</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($citas
                as $filaTemporal) : ?>
                <tr>
                    <td>
                        <?php echo
                        $filaTemporal->id_cita; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->cedula_cita; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->nombre_cita; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->apellido_cita; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->telefono_cita; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->direccion_cita; ?>
                    </td>
                    <td class="text-center">
                        <a href="#" title="Editar Cita">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url(); ?>/citas/eliminar/<?php echo $filaTemporal->id_cita; ?>" title="Eliminar Cita" onclick="return confirm('¿Estas seguro?');" style="color:red;">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    </td>

                </tr>
            <?php endforeach; ?>
        </tbody>

    </table>
<?php else : ?>
    <h1> Dont have Citas<h1>
        <?php endif; ?>