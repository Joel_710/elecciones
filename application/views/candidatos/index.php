<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="<?php echo base_url(); ?>/assets/images/logo.png" alt="" style="width: 200px; height: 200px;">
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <br>
            <h1 style="color:red;"><b>LISTADO DE CANDIDATOS</b></h1>
        </div>
    </div>
</div>
<div class="col-md-4">
    <a href="<?php echo site_url('candidatos/nuevo'); ?>" class="btn btn-primary">
        <i class="glyphicon glyphicon-plus"></i>
        Agregar Candidato
    </a>
</div>
<br>
<br>
<br>
<?php if ($candidatos) : ?>
    <table class="table table=striped table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>DIGNIDAD</th>
                <th>APELLIDO</th>
                <th>NOMBRE</th>
                <th>LATITUD</th>
                <th>LONGITUD</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($candidatos
                as $filaTemporal) : ?>
                <tr>
                    <td>
                        <?php echo
                        $filaTemporal->id_can; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->dignidad_can; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->apellido_can; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->nombre_can; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->latitud_can; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->longitud_can; ?>
                    </td>
                    <td class="text-center">
                        <a href="#" title="Editar Candidato">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url(); ?>/candidatos/eliminar/<?php echo $filaTemporal->id_can; ?>" title="Eliminar Candidato" onclick="return confirm('¿Estas seguro?');" style="color:red;">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    </td>

                </tr>
            <?php endforeach; ?>
        </tbody>

    </table>
<?php else : ?>
    <h1> Dont have Candidatos<h1>
        <?php endif; ?>