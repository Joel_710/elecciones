<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="<?php echo base_url(); ?>/assets/images/logo.png" alt="" style="width: 100%; height: 150px;">
        </div>
    </div>
</div>




<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <br>
            <br>
            <h1 style="color:red;"><b>NUEVO CANDIDATO</b></h1>
            <br>
            <br>
        </div>
    </div>
</div>

<div class="container">
    <form class="" action="<?php echo site_url(); ?>/candidatos/guardar" method="post">
        <div class="row">
            <div class="col-md-4 text-center">
                <label for="dignidad_can">DIGNIDAD:</label>
                <select id="dignidad_can" name="dignidad_can">
                    <option value="Presidente">Presidente</option>
                    <option value="Asambleista Nacional">Asambleista Nacional</option>
                    <option value="Asambleista Provincial">Asambleista Provincial</option>
                </select>
                <br>
            </div>
            <div class="col-md-4 text-center">
                <label for="">APELLIDO:</label>
                <br>
                <input type="text" placeholder="Ingrese sus apellidos" class="form-control" name="apellido_can" value="">
            </div>
            <div class="col-md-4 text-center">
                <label for="">NOMBRE:</label>
                <br>
                <input type="text" placeholder="Ingrese sus nombres" class="form-control" name="nombre_can" value="">
            </div>
            <div class="col-md-4 text-center">
                <label for="">MOVIMIENTO:</label>
                <br>
                <input type="text" placeholder="Ingrese su movimiento" class="form-control" name="movimiento_can" value="">
            </div>
            <div class="col-md-4 text-center">
                <label for="">LATITUD:</label>
                <br>
                <input type="text" placeholder="Seleccione la ubicacion en el mapa" class="form-control" name="latitud_can" id="latitud" value="">
            </div>
            <div class="col-md-4 text-center">
                <label for="">LONGITUD:</label>
                <br>
                <input type="text" placeholder="Seleccione la ubicacion en el mapa" class="form-control" name="longitud_can" id="longitud" value="">
            </div>
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <div id="mapaUbicacion" 
                    style="height: 400px; width:100%;
                    border:2px solid black;"></div>
                    <br>
                </div>
            </div>
            <script type="text/javascript">
                function initMap(){
                    var centro=
                    new google.maps.LatLng(-2.1741112410914147, 
                        -78.914874103013575);
                        var mapa1=new google.maps.Map(
                            document.getElementById('mapaUbicacion'),
                        {
                            center: centro,
                            zoom: 7,
                            mapTypeId:google.maps.MapTypeId.ROADMAP
                        }
                    );

                    var marcador=new google.maps.Marker({
                        position:centro,
                        map:mapa1,
                        title:"Seleccione la dirreccion",
                        icon:"",
                        draggable:true
                    });
                    google.maps.event.addListener(marcador,
                    'dragend', function(){
                        /*alert("Se termino el Drag");*/
                        document.getElementById('latitud').value=
                        this.getPosition().lat();
                        document.getElementById('longitud').value=
                        this.getPosition().lng();
                    });
                }//cierre de la funcion initMap

            </script>
        </div>
        <br>
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-primary">
                GUARDAR
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/candidatos/index" class="btn btn-danger">CANCELAR</a>
        </div>
        <br>

    </form>
</div>
<br>
<br>