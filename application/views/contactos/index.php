<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="<?php echo base_url(); ?>/assets/images/1.jpg" alt="" style="width: 150px; height: 150px;">
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <br>
            <h1 style="color:red;"><b>LISTADO DE CONTACTOS</b></h1>
        </div>
    </div>
</div>
<div class="col-md-4">
    <a href="<?php echo site_url('contactos/nuevo'); ?>" class="btn btn-primary">
        <i class="glyphicon glyphicon-plus"></i>
        Agregar Contacto
    </a>
</div>
<br>
<br>
<br>
<?php if ($contactos) : ?>
    <table class="table table=striped table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRES</th>
                <th>APELLIDOS</th>
                <th>EMAIL</th>
                <th>TELEFONO</th>
                <th>DIRECCION</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contactos
                as $filaTemporal) : ?>
                <tr>
                    <td>
                        <?php echo
                        $filaTemporal->id_con; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->nombres_con; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->apellidos_con; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->email_con; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->telefono_con; ?>
                    </td>
                    <td>
                        <?php echo
                        $filaTemporal->direccion_con; ?>
                    </td>
                    <td class="text-center">
                        <a href="#" title="Editar Contacto">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url(); ?>/contactos/eliminar/<?php echo $filaTemporal->id_con; ?>" title="Eliminar Contacto" onclick="return confirm('¿Estas seguro?');" style="color:red;">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    </td>

                </tr>
            <?php endforeach; ?>
        </tbody>

    </table>
<?php else : ?>
    <h1> Dont have Contactos<h1>
        <?php endif; ?>