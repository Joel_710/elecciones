<br>
<div class="container-fluid" style="background-color: black; color: white;">
  <div class="row">
    <div class="col-md-6">
      <img src="<?php echo base_url(); ?>/assets/images/logo.png" alt="">
      <br>
      <br>
      <p>&copy; 2023 Mi Sitio Web. Todos los derechos reservados.</p>
    </div>
    <div class="col-md-6">
      <ul class="list-inline text-center">
        <br>
        <li>Av. 6 de Diciembre N33-122 y Bosmediano</li>
        <ul>
          <br>
          <li>Quito – Pichincha – Ecuador</li>
        </ul>
        <br>
        <ul>
          <li>(593-2) 381-5410 ext 10 – 11 – 12</li>
        </ul>
      </ul>
    </div>
  </div>
</div>

</body>
</html>
